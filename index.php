<?php

// on importe toutes les classes qui sont dans le vendor
require_once 'vendor/autoload.php';

// connexion à la base de données
$servName = "localhost";
$dbname = "bddname";
$user = "root";
$pass = "";

try {
    $bdd = new PDO("mysql:host=$servName;dbname=$dbname;", $user, $pass);
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    echo 'problème d\'accès à la base de données';
    exit;
}

// configuration de Twig
$loader = new \Twig\Loader\FilesystemLoader('templates');
$twig = new \Twig\Environment($loader, [
    'cache' => false,
]);

// récupération des étudiants
$etudiants = $bdd->prepare("SELECT * FROM etudiant");
$etudiants->execute();
$results = $etudiants->fetchAll();

// rendu de la page
echo $twig->render('index.html.twig', [
    'etudiants' => $results,
]);
